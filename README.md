Script em `R` para 

*  fazer download da grade estatistica de população do Censo Demográfico (IBGE) 
*  salvar arquivos em formato `sf` no servidor do Ipea
*  


Dados originais e documentação estão disponíveis [no site do IBGE](ftp://geoftp.ibge.gov.br/recortes_para_fins_estatisticos/grade_estatistica/censo_2010).